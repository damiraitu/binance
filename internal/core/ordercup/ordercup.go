package ordercup

type OrderCup struct {
	LastUpdateId int32      `json:"lastUpdateId"`
	Bids         [][]string `json:"bids"`
	Asks         [][]string `json:"asks"`
}